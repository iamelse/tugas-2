<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VokalController extends Controller
{
    //
    public function logic(Request $string){
        $str = htmlspecialchars($string->input('text'), true);
        $num = preg_match_all('/[aeiou]/i', $str, $matches);

        echo '"'. $str. '"' .' = '. $num .' yaitu ';
        foreach ($matches[0] as $arr){
            echo ' ' . $arr;
        }
    }


}
