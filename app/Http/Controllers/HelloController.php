<?php

namespace App\Http\Controllers;

class HelloController extends Controller
{
    //
    public function index()
    {
        $hello['hi'] = 'HelloWorld';
        
        return view('sayhello.index', $hello);
    }

}