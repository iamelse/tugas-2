<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrintGanjilGenap extends Controller
{
    public function index()
    {
        return view('ganjilGenap.index');
    }

    public function logic(Request $data)
    {
        $a/*['a']*/ = intval($data->input('a'));
        $b/*['b']*/ = intval($data->input('b'));

        //return view('ganjilGenap.result', $a, $b);
        
        for ($a; $a - 1 < $b; $a++){
            if ($a % 2 === 1) {
                echo '<br>' . 'Angka' .' ' . $a . ' ' . 'adalah ganjil';
                //return view('ganjilGenap.result', $a);
            } elseif ($a % 2 === 0) {
                echo '<br>' . 'Angka' . ' ' . $a . ' ' . 'adalah genap';
                //return view('ganjilGenap.result', $a);
            }
        }
         
    }
}
