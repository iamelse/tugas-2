<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    //
    public function add($a, $b){
        $result = $a + $b;
        return view('result', compact('result'));
    }

    public function proses(Request $string){
        
        $a = $string->input('a');
        $b = $string->input('b');
        $operator = $string->input('operator');
        
        $str = $a . $operator . $b . ';';
        $result = eval('return ' . $str);

        return view('calculator.result', compact('result'));
    }

}