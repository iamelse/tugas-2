<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CalculatorController;
use App\Http\Controllers\HelloController;
use App\Http\Controllers\PrintGanjilGenap;
use App\Http\Controllers\VokalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Base Route */
Route::get('/', function () {
    return view('welcome');
});

/** HelloWorld Route */
Route::get('sayhello/', [HelloController::class, 'index']);

/** Kalkulator Route*/
Route::get('/calculator', function(){
    return view('calculator.index');
});

Route::post('calculator/proses', [CalculatorController::class, 'proses']);

/** Ganjil Genap Route */
Route::get('/ganjilgenap', function(){
    return view('ganjilGenap.index');
});

Route::post('ganjilgenap/logic', [PrintGanjilGenap::class, 'logic']);

/** Vokal Char Route */
Route::get('/vowel', function () {
    return view('vokalchar.index');
});

Route::post('vowel/logic', [VokalController::class, 'logic']);